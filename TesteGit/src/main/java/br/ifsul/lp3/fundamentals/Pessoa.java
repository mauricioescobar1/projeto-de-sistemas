package br.ifsul.lp3.fundamentals;

import java.math.BigDecimal;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "pessoa_mysql")
public class Pessoa {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	private String nomeCompleto;

	@Column(length = 11, nullable = false)
	private String cpf;
	
	@Column(name = "endereco", length = 100)
	private String e;
	
	@Column(precision = 10, scale = 2)
	private BigDecimal salario;
	
	@Column(columnDefinition = "TEXT")
	private String biografia;
}
