package br.ifsul.lp3.many_to_many;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import jakarta.annotation.PostConstruct;

@Component
public class ManyToManyTestes {

	@Autowired
	private AutorRepository ar;
	@Autowired
	private LivroRepository lr;
	
	@PostConstruct
	private void run() {

//		Autor a1 = new Autor();
//		a1.setNome("Mauricio");
//
//		Livro l1 = new Livro();
//		l1.setTitulo("Livro 1");
//
//		a1.getLivros().add(l1);
//		l1.getAutores().add(a1);
//		
//		ar.save( a1 );
		
		Autor a = ar.findById(1).get();
		
		for(Livro l : a.getLivros())
		{
			l.removeAutor(a);
			
			lr.save( l );
		}
	}

}
