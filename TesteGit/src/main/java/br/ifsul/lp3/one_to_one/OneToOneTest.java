package br.ifsul.lp3.one_to_one;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import jakarta.annotation.PostConstruct;

@Component
public class OneToOneTest {

	@Autowired
	private ProfileRepository profileRepository;

	@PostConstruct
	private void run() {
		profileRepository.deleteAll(); // remove todos

		User u = User.builder()
				.username("escobar")
				.password("1234").build();
		
		Profile p = Profile.builder()
				.address("Rua ABC")
				.dateOfBirth("28/03")
				.fullName("Mauricio Escobar").build();

		p.setUser(u);
		
		p = profileRepository.save(p);
		
		System.out.println(p);
	}
}
