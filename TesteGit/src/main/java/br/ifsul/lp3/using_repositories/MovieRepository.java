package br.ifsul.lp3.using_repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MovieRepository extends JpaRepository<Movie, Long> {

	List<Movie> findByTitle(String title);

	Page<Movie> findByTitle(String title, Pageable p);

	List<Movie> findDistinctByTitle(String title);

	List<Movie> findDistinctByTitleContaining(String title);

	Integer countByTitle(String title);

	Integer countByTitleContaining(String title);

	List<Movie> findByTitleAndYear(String title, Integer year);
	
	List<Movie> findFirst3ByTitleContaining(String keyword);
	
	List<Movie> findByYearBetween(Integer start, Integer end);
}
