package br.ifsul.lp3;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import br.ifsul.lp3.one_to_one.Profile;
import br.ifsul.lp3.one_to_one.ProfileRepository;
import br.ifsul.lp3.one_to_one.User;
import br.ifsul.lp3.one_to_one.UserRepository;

@SpringBootTest
class MainTests {

	@Autowired
	private UserRepository userRepository;
	@Autowired
	private ProfileRepository profileRepository;

	@Order(1)
	@Test
	void testUser_firstForm() {
		Profile p = Profile.builder().address("Rua ABC").dateOfBirth("28/03").fullName("Mauricio Escobar").build();
		User u = User.builder().username("escobar").password("1234").build();
		
		userRepository.deleteAll();
		profileRepository.deleteAll();
		
		p.setUser(u);		
		profileRepository.save(p);
				
		assertEquals(userRepository.count(), 1);
		assertEquals(profileRepository.count(), 1);
		assertNotNull(p.getUser());
	}
	
	@Order(2)
	@Test
	void testRemovingProfile() {
		assertEquals(profileRepository.count(), 1);
	}

//	@Autowired
//	private UserRepositoryDefaultValues repository;
//	
//	@Test
//	void testUser_firstForm() {
//		User u = new User();
//		
//		User u2 = new User();
//		u2.setAge(30);
//		u2.setEmailConfirmed(true);
//		
//		u = repository.save( u );
//		u2 = repository.save( u2 );
//		
//		assertEquals(u.getAge(), 1);
//		assertFalse(u.isEmailConfirmed());
//		
//		assertEquals(u2.getAge(), 30);
//		assertTrue(u2.isEmailConfirmed());
//	}

//	@Autowired
//	private MovieRepository movieRepository;
//
//	@Test
//	void usingRepositories() {
//		Movie m1 = new Movie();
//		m1.setTitle("Matrix");
//
//		m1 = movieRepository.save(m1);
//
//		System.out.println(m1.getId());
//
//		assertEquals(m1.getTitle(), "Matrix");
//	}
}
